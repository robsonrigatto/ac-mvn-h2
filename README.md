# Spring boot + H2 + JUnit

## Executar os testes unitários
`mvn test`

## Executar a API
`mvn spring-boot:run` ou `./mvnw spring-boot:run`

## Documentação swagger
> [http://localhost:8080/swagger-ui.html]