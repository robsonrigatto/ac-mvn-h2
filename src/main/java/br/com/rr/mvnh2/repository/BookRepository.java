package br.com.rr.mvnh2.repository;

import br.com.rr.mvnh2.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<BookEntity, Long> {
}
