package br.com.rr.mvnh2.vo;

public class BookVO {

    private Long id;
    private String description;
    private String author;
    private String isbn;
    private String publisher;

    public BookVO() { }

    public BookVO(Long id, String description, String author, String isbn, String publisher) {
        this.id = id;
        this.description = description;
        this.author = author;
        this.isbn = isbn;
        this.publisher = publisher;
    }

    public Long getId() { return id; }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
