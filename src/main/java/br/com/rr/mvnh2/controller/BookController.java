package br.com.rr.mvnh2.controller;

import br.com.rr.mvnh2.entity.BookEntity;
import br.com.rr.mvnh2.repository.BookRepository;
import br.com.rr.mvnh2.vo.BookVO;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("books")
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    @PostMapping
    @Transactional
    public ResponseEntity create(@RequestBody BookVO bookVO, UriComponentsBuilder ucBuilder) {
        BookEntity book = new BookEntity(bookVO.getDescription(), bookVO.getAuthor(), bookVO.getIsbn(), bookVO.getPublisher());
        book = this.bookRepository.save(book);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/book/{id}").buildAndExpand(book.getId()).toUri());

        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable("id") Long id) {
        Optional<BookEntity> bookOpt = bookRepository.findById(id);

        if(!bookOpt.isPresent()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        BookEntity book = bookOpt.get();
        BookVO bookVO = new BookVO(book.getId(), book.getDescription(), book.getAuthor(), book.getIsbn(), book.getPublisher());

        return new ResponseEntity(bookVO, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity findAll() {
        List<BookEntity> books = bookRepository.findAll();

        if(books.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        List<BookVO> booksVO = books.stream()
                .map(book -> new BookVO(book.getId(), book.getDescription(), book.getAuthor(), book.getIsbn(), book.getPublisher()))
                .collect(Collectors.toList());

        return new ResponseEntity(booksVO, HttpStatus.OK);
    }

    @PutMapping("{id}")
    @Transactional
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody BookVO bookVO) {
        Optional<BookEntity> bookOpt = bookRepository.findById(id);

        if(!bookOpt.isPresent()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        BookEntity book = bookOpt.get();
        book.setAuthor(bookVO.getAuthor());
        book.setIsbn(bookVO.getIsbn());
        book.setPublisher(bookVO.getPublisher());
        book = bookRepository.save(book);

        bookVO = new BookVO(book.getId(), book.getDescription(), book.getAuthor(), book.getIsbn(), book.getPublisher());

        return new ResponseEntity(bookVO, HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    @Transactional
    public ResponseEntity delete(@PathVariable("id") Long id) {
        Optional<BookEntity> bookOpt = bookRepository.findById(id);

        if(!bookOpt.isPresent()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        bookRepository.deleteById(bookOpt.get().getId());

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
