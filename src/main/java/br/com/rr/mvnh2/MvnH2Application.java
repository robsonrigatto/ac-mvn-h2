package br.com.rr.mvnh2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvnH2Application {

	public static void main(String[] args) {
		SpringApplication.run(MvnH2Application.class, args);
	}
}
