package br.com.rr.mvnh2;

import br.com.rr.mvnh2.entity.BookEntity;
import br.com.rr.mvnh2.repository.BookRepository;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookRepositoryTest {

	@Autowired
    private BookRepository bookRepository;

	@Test
	public void findAllTest() {
        List<BookEntity> all = bookRepository.findAll();
        Assert.assertTrue(all.isEmpty());

        BookEntity book = new BookEntity(
                "Use A Cabeca! Java", "Kathy Sierra",
                "8576081733", "Alta Books");
        bookRepository.save(book);


        all = bookRepository.findAll();
        Assert.assertFalse(all.isEmpty());
        Assert.assertEquals(1, all.size());
	}

}
